package main;

import afcl.WorkflowManager;
import utils.Measure;

public class Main {

    private static String inputVM = "t2.micro";

	public static void main(String[] args) {
		
        Measure measure1 = new Measure(inputVM, "workflow AFCL enactment engine with Redis cluster");
        
    	WorkflowManager.execAFCLEngine();
    	
    	measure1.stopAndSave();

		
	}

}
