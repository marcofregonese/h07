package utils;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

public class Measure {

//    private final Logger log = Logger.getLogger(Measure.class);
    private String measureResultFilePath = "measure.txt";

    private String measureDescription;
    private String vmType;
    private Instant startTime;
    private Instant endTime;
    private long timeElapsed;

    public Measure(String name) {
        this.vmType = name;
        startTime = Instant.now();
    }
    
    public Measure(String name, String type) {
        this.vmType = name;
        this.measureDescription = type;
        startTime = Instant.now();
    }

    public Measure(String name, String type, String savePath) {
        this.vmType = name;
        this.measureDescription = type;
        measureResultFilePath = savePath;
        startTime = Instant.now();
    }

    public void stop() {
        endTime = Instant.now();
        timeElapsed = Duration.between(startTime, endTime).toMillis();
//        log.info(String.format("Time for %s: %d ms", name, timeElapsed));
    }

    public void stopAndSave() {
        stop();
        save();
    }

    public void save() {
        save(measureResultFilePath);
    }

    public void save(String path) {
        try {
            FileWriter writer = new FileWriter(path, true);

            writer.append(String.format("exec: %s, time taken: %d ms, %d secs, %d min\n", measureDescription, timeElapsed, timeElapsed/1000, timeElapsed/60000));
            System.out.println(String.format("exec: %s, time taken: %d ms, %d secs, %d min\n", measureDescription, timeElapsed, timeElapsed/1000, timeElapsed/60000));
            writer.flush();
            writer.close();
        } catch (IOException e) {
//            log.error(e.getMessage());
        	e.printStackTrace();
        }
    }
}
