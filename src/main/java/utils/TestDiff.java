package utils;

import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import de.danielbechler.diff.node.Visit;
import pojo.Form;
import pojo.RideRequest;

public class TestDiff {

	public static void main(String[] args) {
		
		Form base = new Form();
//		base.setFormID(111);
		Form head = new Form();
		head.setFormID(222);
		RideRequest rr1 = new RideRequest();
		rr1.setRideRequestID(7474);
		rr1.setPassengerName("Marco");
		head.setRideRequest(rr1);
		
		Form modified = new Form();
		modified.setFormID(999);
		rr1.setPassengerName(null);
		rr1.setPassengerTelNum("041997626");
		modified.setRideRequest(rr1);
		
//		printDifferences(base, modified);
//		Form res = getUpdateForm(base, modified);	
//		JSONManager.printPrettyJSON(res);
		
		ObjectMerger om = new ObjectMerger();
		Form ret = om.merge(modified, base, head);
		JSONManager.printPrettyJSON(ret);
		
	}

	public static void printDifferences(Form base, Form updated) {
		
		DiffNode diff = ObjectDifferBuilder.buildDefault().compare(base, updated);
		
		diff.visit(new DiffNode.Visitor()
		{
		    public void node(DiffNode node, Visit visit)
		    {
		        final Object baseValue = node.canonicalGet(base);
		        final Object workingValue = node.canonicalGet(updated);
		        final String message = node.getPath() + " changed from " + 
		                               baseValue + " to " + workingValue;
		        System.out.println(message);
		    }
		});
		
	}
	
	public static Form getUpdateForm(Form base, Form updated) {
		
		Form output = new Form();
		DiffNode diff = ObjectDifferBuilder.buildDefault().compare(base, updated);
		
		diff.visit(new DiffNode.Visitor()
		{
		    public void node(DiffNode node, Visit visit)
		    {
		        // only leaf-nodes with changes
		        if (node.hasChanges() && !node.hasChildren()) {
		            node.canonicalSet(output, node.canonicalGet(updated));
		        }
		    }
		});
		
		return output;
	}
}
