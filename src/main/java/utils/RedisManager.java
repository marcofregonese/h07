package utils;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import pojo.Form;
import pojo.ManageForm;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

public class RedisManager {

	public static JedisCluster getRedisClusterConection(String host, int port) {
    	
    		//			!!!!	REMEMBER TO CHECK HOST		!!!!			*/
		Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();
		//Jedis Cluster will attempt to discover cluster nodes automatically
		jedisClusterNodes.add(new HostAndPort(host, port));
		JedisCluster jc = new JedisCluster(jedisClusterNodes);
		Map<String, JedisPool> map = jc.getClusterNodes();
		System.out.println("cluster nodes: "+map.keySet());
		return  jc;    	
    }
	
	public static Jedis getRedisLocalConnection() {
		
    	// connection to Redis @localhost
    	Jedis jedis = new Jedis();
    	System.out.println(jedis.ping());
    	return jedis;
	}
	
	//Host hardcoded
	public static void insertInitialDataIntoRedisCluster() {
		
		Form input = ManageForm.generateData("Marco", "041997626", "frego@gmail.com");
		String jsonObj = JSONManager.convertPojoIntoJSONString(input);
		JedisCluster jc = getRedisClusterConection("18.197.79.125", 7000);
		jc.set("input", jsonObj);
		
	}
	
	public static void main(String[] args) {
		
//		getRedisClusterConection();
		insertInitialDataIntoRedisCluster();
	}

}
