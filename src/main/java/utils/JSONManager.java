package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import pojo.Form;
import pojo.ManageForm;

public class JSONManager {
	
	public static void printJSON(Form ob) {
		
		ObjectMapper mapper = new ObjectMapper();
		String json = null;
		try {
			json = mapper.writeValueAsString(ob);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(json);
	}

	public static void printPrettyJSON(Object ob) {
		
		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
		String json = null;
		try {
			json = mapper.writeValueAsString(ob);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(json);
		
	}
	
	public static String convertPojoIntoJSONString(Object ob) {
		
//	       JSONObject jsonObj = new JSONObject(ob);
//	       return jsonObj.toString();
		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
		String json = null;
		try {
			json = mapper.writeValueAsString(ob);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json.toString();
		
	}
	
	public static Form convertJSONintoPojo(String jsonText) {
		
    	ObjectMapper mapper = new ObjectMapper();
    	Form form = null;
    	try {
			form = mapper.readValue(jsonText, Form.class);
//			System.out.println(readValue.getFormID());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return form;
	}
	
	public static void main(String[] args) {
		
		Form form = ManageForm.generateData("Marco", "03290933439", "mymail@gmail.com");
		printPrettyJSON(form);
	}
}
