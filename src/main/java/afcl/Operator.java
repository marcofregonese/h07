package afcl;

public interface Operator {
	boolean compare(boolean a, boolean b);
}
