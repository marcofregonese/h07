package afcl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dps.afcl.Function;
import com.dps.afcl.Workflow;
import com.dps.afcl.functions.AtomicFunction;
import com.dps.afcl.functions.IfThenElse;
import com.dps.afcl.functions.Parallel;
import com.dps.afcl.functions.objects.ACondition;
import com.dps.afcl.functions.objects.Section;
import com.dps.afcl.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lambda.InvocationManager;
import pojo.Form;
import pojo.ManageForm;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import utils.JSONManager;
import utils.RedisManager;

public class WorkflowManager {

	//				ATTENTION!!!!    follow this instruction when exporting as runnable jar
	//uncomment following line and comment the one after next and run with following cmd: $ java -jar app.jar path/to/myYAMLfile.yaml
//	private static String pathToYAMLfile = "./H05.yaml";
	
//	relative file path has to be src/ect. without forward slash at the front!!!!! 
	private static final String pathToYAMLfile = "src/main/resources/H05.yaml";
	
	
//	static String JSON_SCHEMA_FILE = "/home/marco/eclipse-workspace/b/src/main/resources/schema.json";
	private static Workflow workflowNew = Utils.readYAMLNoValidation(pathToYAMLfile);
    private static List<Function> body = workflowNew.getWorkflowBody();
	
	public static void execAFCLEngine() {
		
		//remember to modify redis cluster host in H05.yaml file
        String host = workflowNew.getDataIns().get(0).getSource();
        String key = workflowNew.getDataIns().get(0).getName();
        System.out.println("host: "+host);
		JedisCluster jc = RedisManager.getRedisClusterConection(host, 7000);
		String inValue = jc.get(key);
		Form inputForm = JSONManager.convertJSONintoPojo(inValue);
		Form output = manageWorkflow(body, inputForm);
		String outValue = JSONManager.convertPojoIntoJSONString(output);
		jc.set("output", outValue);
		
		//print result
		JSONManager.printPrettyJSON(output);
		
	}
	
	//just for testing purposes
    public static void execLocalRedis(){
    	
    	// connection to Redis @localhost
    	Jedis jedis = new Jedis();
    	System.out.println(jedis.ping());
    	
    	Workflow workflowNew = Utils.readYAMLNoValidation(pathToYAMLfile);
    	System.out.println(workflowNew.getName());
        List<Function> body = workflowNew.getWorkflowBody();
    	
        Form input = ManageForm.generateData("Marco", "041997626", "frego@gmail.com");
        
        // set Redis key with input data just created
        String jsonObj = JSONManager.convertPojoIntoJSONString(input) ;
        jedis.set("input", jsonObj);
//        System.out.println(jedis.get("input"));
        jedis.close();
        
        //Retrieve data from Redis using YAML file
        String dataSource = workflowNew.getDataIns().get(1).getSource();
        System.out.println(dataSource);
    	Jedis jedisBis = new Jedis(dataSource, 6379);
    	String jsonText = jedisBis.get("input");
    	
    	//Convert json into pojo
    	ObjectMapper mapper = new ObjectMapper();
    	Form inputForm = null;
    	try {
			inputForm = mapper.readValue(jsonText, Form.class);
//			System.out.println(readValue.getFormID());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    	Form output = manageWorkflow(body, inputForm);
//    	JSONManager.printPrettyJSON(output.getRideRequest());
//    	JSONManager.printPrettyJSON(output.getOptimal());

    	//Convert Form output into json
    	String value = JSONManager.convertPojoIntoJSONString(output);
    	//Save output into Redis
    	jedisBis.set("output", value);
    	jedisBis.close();
    	System.out.println(jedisBis.get("output"));
    	
    }
    
    public static Form manageWorkflow(List<Function> body, Form input) {
    	
    	Form output = null;
//    	List<Form> outputs = null;
        for(Function f : body) {

        	if(f instanceof AtomicFunction) {
        		System.out.println("atomic function: "+ f.getName()+" is being called...");
        		String functionName = f.getName();
        		output = InvocationManager.invoke(functionName, input);
        		System.out.println("function: "+f.getName()+" called");
//        		JSONManager.printPrettyJSON(output);

        	}
        	else if(f instanceof IfThenElse) {
        		System.out.println("if then else function: "+f.getName()+" is being processed...");
        		IfThenElse ite = (IfThenElse) f;
        		output = manageIfThenElse(ite, input);
        		
        	}
        	else if(f instanceof Parallel){
        		System.out.println("parallel functions: "+f.getName()+" is being processed...");
        		Parallel par = (Parallel) f;
        		output = manageParallel(par, input);
//        		JSONManager.printPrettyJSON(output);
        	}
        	input = output;
        }
        return input;
    }
    
    public static Form manageIfThenElse(IfThenElse function, Form input) {
    	
        Map<String, Operator> opMap = new HashMap<String, Operator>();
        opMap.put("==", new Operator() {
            @Override public boolean compare(boolean a, boolean b) {
                return a == b;
            }
        });
    	boolean newdata = input.isNewOutputdata();
    	List<ACondition> conditions = function.getCondition().getConditions();
    	String operator = conditions.get(0).getOperator();
//    	String data1 = conditions.get(0).getData1();
    	
    	Form output = null;
    	
    	if(opMap.get(operator).compare(false, newdata)) {
    		List<Function> elseBranch = ((IfThenElse) function).getElse();
    		output = manageWorkflow(elseBranch, input);
    	}else {
    		List<Function> thenBranch = ((IfThenElse) function).getThen();
    		output = manageWorkflow(thenBranch, input);
    	}
    	
		return output;
		
    }
    
    
    public static Form manageParallel(Parallel parallel, Form input) {
    	
       	List<Section> body = parallel.getParallelBody();
       	int bodysize = body.size();
       	System.out.println("parallel body size: "+ bodysize);
       	List<Thread> threads = new ArrayList<Thread>();
       	List<Form> outputs = new ArrayList<Form>();
       	
        for(Section f : body) {
        	
    		String functionName = f.getSection().get(0).getName();
    		threads.add(new Thread() {
    			   public void run() { 
    				  System.out.println(functionName+" is being called...");
    			      Form output = InvocationManager.invoke(functionName, input); 
    			      outputs.add(output);
    			      System.out.println("parallel function: "+functionName+" called");
    			   }
    			});
        }
        
//        start threads
        for(Thread t : threads) {
        	t.start();
        }
        
//        waits for threads to finish
        for(Thread t : threads) {
        	try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
        
        String parallelName = parallel.getName();
        System.out.println(parallelName);
        Form output = InvocationManager.invoke(parallelName, outputs);
//        JSONManager.printPrettyJSON(output.getPossibleOffers());
    	return output;
	
    }

}

