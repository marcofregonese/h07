package lambda;
import java.util.List;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

import pojo.Form;
import pojotest.RequestClass;
import pojotest.ResponseClass;

public interface Service {
  @LambdaFunction(functionName="pojoHandler")
  ResponseClass pojoHandler(RequestClass Form);
  @LambdaFunction(functionName="f0")
  Form f0(Form input);
  @LambdaFunction(functionName="selectAllRiderOffers")
  Form selectAllRiderOffers(Form input);
  @LambdaFunction(functionName="checkMatches")
  Form checkMatches(Form input);
  @LambdaFunction(functionName="calculateCost")
  Form calculateCost(Form input);
  @LambdaFunction(functionName="calculateWaitingTime")
  Form calculateWaitingTime(Form input);
  @LambdaFunction(functionName="calculateTravelTime")
  Form calculateTravelTime(Form input);
  @LambdaFunction(functionName="parallelf3f4f5")
  Form parallelf3f4f5(List<Form> input);
  @LambdaFunction(functionName="findOptimal")
  Form findOptimal(Form input);
  @LambdaFunction(functionName="determinePickupTimePlace")
  Form determinePickupTimePlace(Form input);
  @LambdaFunction(functionName="informPassenger")
  Form informPassenger(Form input);
  @LambdaFunction(functionName="informDriver")
  Form informDriver(Form input);
  @LambdaFunction(functionName="parallelf8f9")
  Form parallelf8f9(List<Form> input);
  @LambdaFunction(functionName="logIntoDB")
  Form loginIntoDB(Form input);
  
}
