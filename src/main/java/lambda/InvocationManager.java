package lambda;

import java.util.List;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;

import pojo.Form;

public class InvocationManager {

	public static Form invoke(String functionName, Form input) {
		
		AWSLambdaClientBuilder builder = AWSLambdaClientBuilder.standard()
				.withRegion("eu-central-1");
		AWSLambda client = builder.build();

		final Service service = LambdaInvokerFactory.builder()
								.lambdaClient(client)
								.build(Service.class);
		
		Form out = null;
		switch(functionName) {
		
			case "selectAllRiderOffers":
				out = service.selectAllRiderOffers(input);
				break;
			case "checkMatches":
				out = service.checkMatches(input);
				break;
			case "calculateCost":
				out = service.calculateCost(input);
				break;
			case "calculateTravelTime":
				out = service.calculateTravelTime(input);
				break;
			case "calculateWaitingTime":
				out = service.calculateWaitingTime(input);
				break;
			case "determinePickupTimePlace":
				out = service.determinePickupTimePlace(input);
				break;
			case "findOptimal":
				out = service.findOptimal(input);
				break;
			case "informDriver":
				out = service.informDriver(input);
				break;
			case "informPassenger":
				out = service.informPassenger(input);
				break;
			case "logIntoDB":
				out = service.loginIntoDB(input);
				break;
			case "f0":
				out = service.f0(input);
				break;
			default:
				System.out.println("no match in lambda invocation input Form");

		}
		
		return out;
		
	}
	
	public static Form invoke(String functionName, List<Form> input) {
		
		AWSLambdaClientBuilder builder = AWSLambdaClientBuilder.standard()
				.withRegion("eu-central-1");
		AWSLambda client = builder.build();

		final Service service = LambdaInvokerFactory.builder()
								.lambdaClient(client)
								.build(Service.class);
		
		Form out = null;
		switch(functionName) {
		
		case "parallelf3f4f5":
			out = service.parallelf3f4f5(input);
			break;
		case "parallelf8f9":
			out = service.parallelf8f9(input);
			break;
		default:
			System.out.println("no match in lambda invocation input List<Form>");
		}
		return out;
	}

}
