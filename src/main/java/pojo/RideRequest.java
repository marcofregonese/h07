package pojo;

public class RideRequest {
	
	private int rideRequestID;
	private String passengerName;
	private String passengerTelNum;
	private String passengerEmail;
	private Location startPoint;
	private Location endPoint;
//	private LocalTime availableAtTime;
	
	
	public RideRequest() {
		super();
	}
	public String getPassengerTelNum() {
		return passengerTelNum;
	}
	public void setPassengerTelNum(String passengerTelNum) {
		this.passengerTelNum = passengerTelNum;
	}
	public String getPassengerEmail() {
		return passengerEmail;
	}
	public void setPassengerEmail(String passengerEmail) {
		this.passengerEmail = passengerEmail;
	}
//	public LocalTime getAvailableAtTime() {
//		return availableAtTime;
//	}
//	public void setAvailableAtTime(LocalTime leavingAtTime) {
//		this.availableAtTime = leavingAtTime;
//	}
	public Location getStartPoint() {
		return startPoint;
	}
	public void setStartPoint(Location startPoint) {
		this.startPoint = startPoint;
	}
	public Location getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(Location endPoint) {
		this.endPoint = endPoint;
	}
	public int getRideRequestID() {
		return rideRequestID;
	}
	public void setRideRequestID(int rideRequestID) {
		this.rideRequestID = rideRequestID;
	}
	public String getPassengerName() {
		return passengerName;
	}
	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}
}
