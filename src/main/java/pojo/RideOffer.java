package pojo;

import java.time.LocalDateTime;

public class RideOffer {
	
	private int rideOfferID;
	private String driverName;
	private String driverTelNum;
	private String driverEmail;
	private Location startPoint;
	private Location endPoint;
	private LocalDateTime leavingAtTime;
	private double priceKm;
	private int speed;
	private double rideCost;
	private LocalDateTime leavingTime;
	private int waitingTimeMinutes;
	private int travelDurationMinutes;
	private Location pickupPlace;
	private LocalDateTime pickupTime;
	
	
	public String getDriverTelNum() {
		return driverTelNum;
	}
	public void setDriverTelNum(String driverTelNum) {
		this.driverTelNum = driverTelNum;
	}
	public String getDriverEmail() {
		return driverEmail;
	}
	public void setDriverEmail(String driverEmail) {
		this.driverEmail = driverEmail;
	}
	public int getWaitingTimeMinutes() {
		return waitingTimeMinutes;
	}
	public void setWaitingTimeMinutes(int waitingTimeMinutes) {
		this.waitingTimeMinutes = waitingTimeMinutes;
	}
	public int getTravelDurationMinutes() {
		return travelDurationMinutes;
	}
	public void setTravelDurationMinutes(int travelDurationMinutes) {
		this.travelDurationMinutes = travelDurationMinutes;
	}
	public LocalDateTime getLeavingAtTime() {
		return leavingAtTime;
	}
	public void setLeavingAtTime(LocalDateTime leavingAtTime) {
		this.leavingAtTime = leavingAtTime;
	}
	public LocalDateTime getLeavingTime() {
		return leavingTime;
	}
	public void setLeavingTime(LocalDateTime leavingTime) {
		this.leavingTime = leavingTime;
	}
	public LocalDateTime getPickupTime() {
		return pickupTime;
	}
	public void setPickupTime(LocalDateTime pickupTime) {
		this.pickupTime = pickupTime;
	}
	public double getPriceKm() {
		return priceKm;
	}
	public void setPriceKm(double priceKm) {
		this.priceKm = priceKm;
	}
	public Location getStartPoint() {
		return startPoint;
	}
	public void setStartPoint(Location startPoint) {
		this.startPoint = startPoint;
	}
	public Location getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(Location endPoint) {
		this.endPoint = endPoint;
	}
	public Location getPickupPlace() {
		return pickupPlace;
	}
	public void setPickupPlace(Location pickupPlace) {
		this.pickupPlace = pickupPlace;
	}
	public int getRideOfferID() {
		return rideOfferID;
	}
	public void setRideOfferID(int rideOfferID) {
		this.rideOfferID = rideOfferID;
	}
	public double getRideCost() {
		return rideCost;
	}
	public void setRideCost(double rideCost) {
		this.rideCost = rideCost;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}

}
