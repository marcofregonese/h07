package pojo;

import java.time.LocalDateTime;
import java.util.Collection;

public class Form {
	
	private int formID;
	private String message = null;
	private Collection<RideOffer> rideOffers;
	private RideRequest rideRequest;
	private Collection<RideOffer> possibleOffers;
	private RideOffer optimal;
	private Location pickupLocation;
	private LocalDateTime pickTime;
	private boolean messageSentToPassenger;
	private boolean messageSentToDriver;
	private boolean newOutputdata = false;
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Form() {
		super();
	}
	public boolean isNewOutputdata() {
		return newOutputdata;
	}
	public void setNewOutputdata(boolean newOutputdata) {
		this.newOutputdata = newOutputdata;
	}
	public boolean isMessageSentToPassenger() {
		return messageSentToPassenger;
	}
	public void setMessageSentToPassenger(boolean messageSentToPassenger) {
		this.messageSentToPassenger = messageSentToPassenger;
	}
	public boolean isMessageSentToDriver() {
		return messageSentToDriver;
	}
	public void setMessageSentToDriver(boolean messageSentToDriver) {
		this.messageSentToDriver = messageSentToDriver;
	}
	public LocalDateTime getPickTime() {
		return pickTime;
	}
	public void setPickTime(LocalDateTime pickTime) {
		this.pickTime = pickTime;
	}
	public Location getPickupLocation() {
		return pickupLocation;
	}
	public void setPickupLocation(Location pickupLocation) {
		this.pickupLocation = pickupLocation;
	}
	public Collection<RideOffer> getPossibleOffers() {
		return possibleOffers;
	}
	public void setPossibleOffers(Collection<RideOffer> possibleOffers) {
		this.possibleOffers = possibleOffers;
	}
	public RideRequest getRideRequest() {
		return rideRequest;
	}
	public void setRideRequest(RideRequest rideRequest) {
		this.rideRequest = rideRequest;
	}
	public int getFormID() {
		return formID;
	}
	public void setFormID(int formID) {
		this.formID = formID;
	}
	public Collection<RideOffer> getRideOffers() {
		return rideOffers;
	}
	public void setRideOffers(Collection<RideOffer> rideOffers) {
		this.rideOffers = rideOffers;
	}
	public RideOffer getOptimal() {
		return optimal;
	}
	public void setOptimal(RideOffer optimal) {
		this.optimal = optimal;
	}
	
}


