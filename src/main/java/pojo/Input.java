package pojo;

import java.time.LocalDateTime;
import java.util.Collection;

public class Input {
	
	private int inputID;
	private Collection<RideOffer> rideOffers;
	private RideRequest rideRequest;
	private Collection<RideOffer> possibleOffers;
	private RideOffer optimal;
	private Location pickupLocation;
	private LocalDateTime pickTime;
	
	
	public LocalDateTime getPickTime() {
		return pickTime;
	}
	public void setPickTime(LocalDateTime pickTime) {
		this.pickTime = pickTime;
	}
	public Location getPickupLocation() {
		return pickupLocation;
	}
	public void setPickupLocation(Location pickupLocation) {
		this.pickupLocation = pickupLocation;
	}
	public Collection<RideOffer> getPossibleOffers() {
		return possibleOffers;
	}
	public void setPossibleOffers(Collection<RideOffer> possibleOffers) {
		this.possibleOffers = possibleOffers;
	}
	public RideRequest getRideRequest() {
		return rideRequest;
	}
	public void setRideRequest(RideRequest rideRequest) {
		this.rideRequest = rideRequest;
	}
	public int getInputID() {
		return inputID;
	}
	public void setInputID(int inputID) {
		this.inputID = inputID;
	}
	public Collection<RideOffer> getRideOffers() {
		return rideOffers;
	}
	public void setRideOffers(Collection<RideOffer> rideOffers) {
		this.rideOffers = rideOffers;
	}
	public RideOffer getOptimal() {
		return optimal;
	}
	public void setOptimal(RideOffer optimal) {
		this.optimal = optimal;
	}
	
}
