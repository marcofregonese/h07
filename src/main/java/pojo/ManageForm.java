package pojo;

import java.util.ArrayList;
import java.util.List;

import utils.JSONManager;

public class ManageForm {
	
	private static int rideRequestID = 0;
	private static int rideOfferID = 0;
	private static int formID = 100;
//	private static LocalDateTime leavingAtTime = LocalDateTime.now();
	
	public static void main(String[] args) {
		
		Form form = generateData("Marco", "041997626", "me@gmail.com");
		JSONManager.printPrettyJSON(form);
	}
	
	public static Form generateData(String name, String telNumber, String mailAddress) {
		
		Location startPoint = createLocation("Spinea", 180, 890);
		Location endPoint = createLocation("Bologna", 150, 700);
		RideRequest rr = ManageForm.createRideRequest(name, telNumber, mailAddress, startPoint, endPoint);
		List<RideOffer> offers = ManageForm.createListOfRideOffers();
		Form input = ManageForm.createFormWithRideOffers(rr, offers);
		input.setMessage("ready");
		return input;
	}
	
	public static Form createFormWithRideOffers(RideRequest rideRequest, List<RideOffer> offers) {
		
		Form  form = new Form();
		form.setFormID(formID++);
		form.setRideRequest(rideRequest);
		form.setRideOffers(offers);
		form.setNewOutputdata(true);
		return form;
	}
	
	public static RideRequest createRideRequest(String passengerName, String passengerTelNum, String passengerEmail,
			Location startLocation, Location endLocation) {
		
//		LocalTime leavingAtTime = LocalTime.now();
		RideRequest rr = new RideRequest();
		rr.setRideRequestID(rideRequestID++);
		rr.setPassengerName(passengerName);
		rr.setPassengerTelNum(passengerTelNum);
		rr.setPassengerEmail(passengerEmail);
		rr.setStartPoint(startLocation);
		rr.setEndPoint(endLocation);
//		rr.setAvailableAtTime(leavingAtTime);
		return rr;
	}
	
	public static RideOffer createRideOffer(String driverName, String driverTelNum, String driverEmail, Location startPoint, Location endPoint, double priceKm) {
		
//		LocalDateTime leavingAtTime = LocalDateTime.now();
		RideOffer ro = new RideOffer();
		ro.setRideOfferID(rideOfferID++);
		ro.setDriverName(driverName);
		ro.setDriverTelNum(driverTelNum);
		ro.setDriverEmail(driverEmail);
		ro.setStartPoint(startPoint);
		ro.setEndPoint(endPoint);
//		ro.setLeavingAtTime(leavingAtTime);
		ro.setPriceKm(priceKm);
		ro.setSpeed(110);
		return ro;
	}
	
	public static List<RideOffer> createListOfRideOffers(){
		
		List<RideOffer> offers = new ArrayList<RideOffer>();
		Location palermo = createLocation("Palermo", 50, 0);
		Location napoli = createLocation("Napoli", 300, 300);
		Location venezia = createLocation("Venezia", 200, 900);
		Location roma = createLocation("Roma", 100, 500);
		Location firenze = createLocation("Firenze", 0, 700);
		Location milano = createLocation("Milano", 100, 1000);
		Location trieste = createLocation("Trieste", 500, 850);
		Location trapani = createLocation("Palermo", 0, 70);
		Location avellino = createLocation("Napoli", 270, 350);
		Location treviso = createLocation("Venezia", 190, 920);
		Location latina = createLocation("Roma", 100, 550);
		Location pisa = createLocation("Firenze", 20, 730);
		Location bergamo = createLocation("Milano", 150, 1000);
		Location udine = createLocation("Trieste", 440, 990);
		Location torino = createLocation("Torino", 440, 990);
		
		offers.add(createRideOffer("Gigio", "041997626", "gigio@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Gino", "03376327", "gino@gmail.com", roma, venezia, 0.34));
		offers.add(createRideOffer("Giovanni", "0411231226", "gio@gmail.com", napoli, trieste, 0.76));
		offers.add(createRideOffer("Luca", "057567657", "luca@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Fabio", "79798564", "fabio@gmail.com", milano, trieste, 0.98));
		offers.add(createRideOffer("Dino", "435657", "dino@gmail.com", trieste, palermo, 0.67));
		offers.add(createRideOffer("Albino", "0411231226", "albino@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alfredo", "02327657", "alfredo@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Gigietto", "041997626", "gigio@gmail.com", venezia, palermo, 0.93));
		offers.add(createRideOffer("Ginetto", "03376327", "gino@gmail.com", venezia, roma, 0.34));
		offers.add(createRideOffer("Giovannino", "0411231226", "gio@gmail.com", trieste, napoli, 0.76));
		offers.add(createRideOffer("Luchetto", "057567657", "luca@gmail.com", milano, palermo, 0.73));
		offers.add(createRideOffer("Fabioetto", "79798564", "fabio@gmail.com", trieste, milano, 0.98));
		offers.add(createRideOffer("Dinetto", "435657", "dino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Albinino", "0411231226", "albino@gmail.com", napoli, milano, 0.43));
		offers.add(createRideOffer("Alfredino", "02327657", "alfredo@gmail.com", avellino, firenze, 0.53));
		offers.add(createRideOffer("Alberto", "041997626", "baby@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Alfio", "03376327", "alfy@gmail.com", roma, treviso, 0.44));
		offers.add(createRideOffer("Arlechino", "0411231226", "arle@gmail.com", latina, trieste, 0.76));
		offers.add(createRideOffer("Artemide", "057567657", "arty@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Assenzio", "79798564", "assy@gmail.com", bergamo, trieste, 0.68));
		offers.add(createRideOffer("Astronzio", "435657", "stronzy@gmail.com", udine, palermo, 0.67));
		offers.add(createRideOffer("Affare", "0411231226", "affa@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alimortaccitua", "02327657", "morty@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Banano", "041997626", "bano@gmail.com", treviso, palermo, 0.93));
		offers.add(createRideOffer("Bieco", "03376327", "bieco@gmail.com", venezia, roma, 0.54));
		offers.add(createRideOffer("Bieno", "0411231226", "by@gmail.com", trieste, trapani, 0.76));
		offers.add(createRideOffer("Busone", "057567657", "busy@gmail.com", milano, palermo, 0.43));
		offers.add(createRideOffer("Boh", "79798564", "boh@gmail.com", udine, milano, 0.98));
		offers.add(createRideOffer("Bino", "435657", "bino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Baucco", "0411231226", "bau@gmail.com", napoli, bergamo, 0.43));
		offers.add(createRideOffer("Balordo", "02327657", "baloo@gmail.com", avellino, firenze, 0.59));
		
		offers.add(createRideOffer("Claudio", "041997626", "cla@gmail.com", treviso, venezia, 0.93));
		offers.add(createRideOffer("Candido", "03376327", "can@gmail.com", roma, avellino, 0.77));
		offers.add(createRideOffer("Clodio", "0411231226", "clo@gmail.com", napoli, trieste, 0.76));
		offers.add(createRideOffer("Cristian", "057567657", "cri@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Cromo", "79798564", "cro@gmail.com", torino, trieste, 0.98));
		offers.add(createRideOffer("Cristiano", "435657", "dino@gmail.com", trieste, palermo, 0.67));
		offers.add(createRideOffer("Clara", "0411231226", "cla@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alfredo", "02327657", "alfredo@gmail.com", firenze, napoli, 0.66));
		offers.add(createRideOffer("Gigietto", "041997626", "gigio@gmail.com", venezia, palermo, 0.93));
		offers.add(createRideOffer("Ginetto", "03376327", "gino@gmail.com", torino, roma, 0.34));
		offers.add(createRideOffer("Giovannino", "0411231226", "gio@gmail.com", trieste, napoli, 0.76));
		offers.add(createRideOffer("Luchetto", "057567657", "luca@gmail.com", milano, palermo, 0.73));
		offers.add(createRideOffer("Fabietto", "79798564", "fabio@gmail.com", trieste, milano, 0.98));
		offers.add(createRideOffer("Fnocchio", "435657", "dino@gmail.com", palermo, trieste, 0.89));
		offers.add(createRideOffer("Albinino", "0411231226", "albino@gmail.com", napoli, milano, 0.43));
		offers.add(createRideOffer("Alfredino", "02327657", "alfredo@gmail.com", avellino, firenze, 0.53));
		offers.add(createRideOffer("Alberto", "041997626", "baby@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Alfio", "03376327", "alfy@gmail.com", roma, treviso, 0.44));
		offers.add(createRideOffer("Arlechino", "0411231226", "arle@gmail.com", latina, trieste, 0.76));
		offers.add(createRideOffer("Artrite", "057567657", "arty@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Assenzio", "79798564", "assy@gmail.com", bergamo, torino, 0.68));
		offers.add(createRideOffer("Astronzio", "435657", "stronzy@gmail.com", udine, palermo, 0.67));
		offers.add(createRideOffer("Affare", "0411231226", "affa@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alimortaccitua", "02327657", "morty@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Bucio", "041997626", "bano@gmail.com", trapani, palermo, 0.93));
		offers.add(createRideOffer("Bieco", "03376327", "bieco@gmail.com", venezia, roma, 0.54));
		offers.add(createRideOffer("Bieno", "0411231226", "by@gmail.com", trieste, trapani, 0.76));
		offers.add(createRideOffer("Busone", "057567657", "busy@gmail.com", pisa, palermo, 0.43));
		offers.add(createRideOffer("Boh", "79798564", "boh@gmail.com", torino, milano, 0.98));
		offers.add(createRideOffer("Bino", "435657", "bino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Baucco", "0411231226", "bau@gmail.com", napoli, bergamo, 0.43));
		offers.add(createRideOffer("Zeno", "02327657", "zen@gmail.com", avellino, firenze, 0.59));
		
		offers.add(createRideOffer("Gigio", "041997626", "gigio@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Gino", "03376327", "gino@gmail.com", roma, venezia, 0.34));
		offers.add(createRideOffer("Giovanni", "0411231226", "gio@gmail.com", napoli, trieste, 0.76));
		offers.add(createRideOffer("Luca", "057567657", "luca@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Fabio", "79798564", "fabio@gmail.com", milano, trieste, 0.98));
		offers.add(createRideOffer("Dino", "435657", "dino@gmail.com", trieste, palermo, 0.67));
		offers.add(createRideOffer("Albino", "0411231226", "albino@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alfredo", "02327657", "alfredo@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Gigietto", "041997626", "gigio@gmail.com", venezia, palermo, 0.93));
		offers.add(createRideOffer("Ginetto", "03376327", "gino@gmail.com", venezia, roma, 0.34));
		offers.add(createRideOffer("Giovannino", "0411231226", "gio@gmail.com", trieste, napoli, 0.76));
		offers.add(createRideOffer("Luchetto", "057567657", "luca@gmail.com", milano, palermo, 0.73));
		offers.add(createRideOffer("Fabioetto", "79798564", "fabio@gmail.com", trieste, milano, 0.98));
		offers.add(createRideOffer("Dinetto", "435657", "dino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Albinino", "0411231226", "albino@gmail.com", napoli, milano, 0.43));
		offers.add(createRideOffer("Alfredino", "02327657", "alfredo@gmail.com", avellino, firenze, 0.53));
		offers.add(createRideOffer("Alberto", "041997626", "baby@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Alfio", "03376327", "alfy@gmail.com", roma, treviso, 0.44));
		offers.add(createRideOffer("Arlechino", "0411231226", "arle@gmail.com", latina, trieste, 0.76));
		offers.add(createRideOffer("Artemide", "057567657", "arty@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Assenzio", "79798564", "assy@gmail.com", bergamo, trieste, 0.68));
		offers.add(createRideOffer("Astronzio", "435657", "stronzy@gmail.com", udine, palermo, 0.67));
		offers.add(createRideOffer("Affare", "0411231226", "affa@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alimortaccitua", "02327657", "morty@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Banano", "041997626", "bano@gmail.com", treviso, palermo, 0.93));
		offers.add(createRideOffer("Bieco", "03376327", "bieco@gmail.com", venezia, roma, 0.54));
		offers.add(createRideOffer("Bieno", "0411231226", "by@gmail.com", trieste, trapani, 0.76));
		offers.add(createRideOffer("Busone", "057567657", "busy@gmail.com", milano, palermo, 0.43));
		offers.add(createRideOffer("Boh", "79798564", "boh@gmail.com", udine, milano, 0.98));
		offers.add(createRideOffer("Bino", "435657", "bino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Baucco", "0411231226", "bau@gmail.com", napoli, bergamo, 0.43));
		offers.add(createRideOffer("Balordo", "02327657", "baloo@gmail.com", avellino, firenze, 0.59));
		
		offers.add(createRideOffer("Claudio", "041997626", "cla@gmail.com", treviso, venezia, 0.93));
		offers.add(createRideOffer("Candido", "03376327", "can@gmail.com", roma, avellino, 0.77));
		offers.add(createRideOffer("Clodio", "0411231226", "clo@gmail.com", napoli, trieste, 0.76));
		offers.add(createRideOffer("Cristian", "057567657", "cri@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Cromo", "79798564", "cro@gmail.com", torino, trieste, 0.98));
		offers.add(createRideOffer("Cristiano", "435657", "dino@gmail.com", trieste, palermo, 0.67));
		offers.add(createRideOffer("Clara", "0411231226", "cla@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alfredo", "02327657", "alfredo@gmail.com", firenze, napoli, 0.66));
		offers.add(createRideOffer("Gigietto", "041997626", "gigio@gmail.com", venezia, palermo, 0.93));
		offers.add(createRideOffer("Ginetto", "03376327", "gino@gmail.com", torino, roma, 0.34));
		offers.add(createRideOffer("Giovannino", "0411231226", "gio@gmail.com", trieste, napoli, 0.76));
		offers.add(createRideOffer("Luchetto", "057567657", "luca@gmail.com", milano, palermo, 0.73));
		offers.add(createRideOffer("Fabietto", "79798564", "fabio@gmail.com", trieste, milano, 0.98));
		offers.add(createRideOffer("Fnocchio", "435657", "dino@gmail.com", palermo, trieste, 0.89));
		offers.add(createRideOffer("Albinino", "0411231226", "albino@gmail.com", napoli, milano, 0.43));
		offers.add(createRideOffer("Alfredino", "02327657", "alfredo@gmail.com", avellino, firenze, 0.53));
		offers.add(createRideOffer("Alberto", "041997626", "baby@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Alfio", "03376327", "alfy@gmail.com", roma, treviso, 0.44));
		offers.add(createRideOffer("Arlechino", "0411231226", "arle@gmail.com", latina, trieste, 0.76));
		offers.add(createRideOffer("Artrite", "057567657", "arty@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Assenzio", "79798564", "assy@gmail.com", bergamo, torino, 0.68));
		offers.add(createRideOffer("Astronzio", "435657", "stronzy@gmail.com", udine, palermo, 0.67));
		offers.add(createRideOffer("Affare", "0411231226", "affa@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alimortaccitua", "02327657", "morty@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Bucio", "041997626", "bano@gmail.com", trapani, palermo, 0.93));
		offers.add(createRideOffer("Bieco", "03376327", "bieco@gmail.com", venezia, roma, 0.54));
		offers.add(createRideOffer("Bieno", "0411231226", "by@gmail.com", trieste, trapani, 0.76));
		offers.add(createRideOffer("Busone", "057567657", "busy@gmail.com", pisa, palermo, 0.43));
		offers.add(createRideOffer("Boh", "79798564", "boh@gmail.com", torino, milano, 0.98));
		offers.add(createRideOffer("Bino", "435657", "bino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Baucco", "0411231226", "bau@gmail.com", napoli, bergamo, 0.43));
		offers.add(createRideOffer("Zeno", "02327657", "zen@gmail.com", avellino, firenze, 0.59));
		
		offers.add(createRideOffer("Gigio", "041997626", "gigio@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Gino", "03376327", "gino@gmail.com", roma, venezia, 0.34));
		offers.add(createRideOffer("Giovanni", "0411231226", "gio@gmail.com", napoli, trieste, 0.76));
		offers.add(createRideOffer("Luca", "057567657", "luca@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Fabio", "79798564", "fabio@gmail.com", milano, trieste, 0.98));
		offers.add(createRideOffer("Dino", "435657", "dino@gmail.com", trieste, palermo, 0.67));
		offers.add(createRideOffer("Albino", "0411231226", "albino@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alfredo", "02327657", "alfredo@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Gigietto", "041997626", "gigio@gmail.com", venezia, palermo, 0.93));
		offers.add(createRideOffer("Ginetto", "03376327", "gino@gmail.com", venezia, roma, 0.34));
		offers.add(createRideOffer("Giovannino", "0411231226", "gio@gmail.com", trieste, napoli, 0.76));
		offers.add(createRideOffer("Luchetto", "057567657", "luca@gmail.com", milano, palermo, 0.73));
		offers.add(createRideOffer("Fabioetto", "79798564", "fabio@gmail.com", trieste, milano, 0.98));
		offers.add(createRideOffer("Dinetto", "435657", "dino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Albinino", "0411231226", "albino@gmail.com", napoli, milano, 0.43));
		offers.add(createRideOffer("Alfredino", "02327657", "alfredo@gmail.com", avellino, firenze, 0.53));
		offers.add(createRideOffer("Alberto", "041997626", "baby@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Alfio", "03376327", "alfy@gmail.com", roma, treviso, 0.44));
		offers.add(createRideOffer("Arlechino", "0411231226", "arle@gmail.com", latina, trieste, 0.76));
		offers.add(createRideOffer("Artemide", "057567657", "arty@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Assenzio", "79798564", "assy@gmail.com", bergamo, trieste, 0.68));
		offers.add(createRideOffer("Astronzio", "435657", "stronzy@gmail.com", udine, palermo, 0.67));
		offers.add(createRideOffer("Affare", "0411231226", "affa@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alimortaccitua", "02327657", "morty@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Banano", "041997626", "bano@gmail.com", treviso, palermo, 0.93));
		offers.add(createRideOffer("Bieco", "03376327", "bieco@gmail.com", venezia, roma, 0.54));
		offers.add(createRideOffer("Bieno", "0411231226", "by@gmail.com", trieste, trapani, 0.76));
		offers.add(createRideOffer("Busone", "057567657", "busy@gmail.com", milano, palermo, 0.43));
		offers.add(createRideOffer("Boh", "79798564", "boh@gmail.com", udine, milano, 0.98));
		offers.add(createRideOffer("Bino", "435657", "bino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Baucco", "0411231226", "bau@gmail.com", napoli, bergamo, 0.43));
		offers.add(createRideOffer("Balordo", "02327657", "baloo@gmail.com", avellino, firenze, 0.59));
		
		offers.add(createRideOffer("Claudio", "041997626", "cla@gmail.com", treviso, venezia, 0.93));
		offers.add(createRideOffer("Candido", "03376327", "can@gmail.com", roma, avellino, 0.77));
		offers.add(createRideOffer("Clodio", "0411231226", "clo@gmail.com", napoli, trieste, 0.76));
		offers.add(createRideOffer("Cristian", "057567657", "cri@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Cromo", "79798564", "cro@gmail.com", torino, trieste, 0.98));
		offers.add(createRideOffer("Cristiano", "435657", "dino@gmail.com", trieste, palermo, 0.67));
		offers.add(createRideOffer("Clara", "0411231226", "cla@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alfredo", "02327657", "alfredo@gmail.com", firenze, napoli, 0.66));
		offers.add(createRideOffer("Gigietto", "041997626", "gigio@gmail.com", venezia, palermo, 0.93));
		offers.add(createRideOffer("Ginetto", "03376327", "gino@gmail.com", torino, roma, 0.34));
		offers.add(createRideOffer("Giovannino", "0411231226", "gio@gmail.com", trieste, napoli, 0.76));
		offers.add(createRideOffer("Luchetto", "057567657", "luca@gmail.com", milano, palermo, 0.73));
		offers.add(createRideOffer("Fabietto", "79798564", "fabio@gmail.com", trieste, milano, 0.98));
		offers.add(createRideOffer("Fnocchio", "435657", "dino@gmail.com", palermo, trieste, 0.89));
		offers.add(createRideOffer("Albinino", "0411231226", "albino@gmail.com", napoli, milano, 0.43));
		offers.add(createRideOffer("Alfredino", "02327657", "alfredo@gmail.com", avellino, firenze, 0.53));
		offers.add(createRideOffer("Alberto", "041997626", "baby@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Alfio", "03376327", "alfy@gmail.com", roma, treviso, 0.44));
		offers.add(createRideOffer("Arlechino", "0411231226", "arle@gmail.com", latina, trieste, 0.76));
		offers.add(createRideOffer("Artrite", "057567657", "arty@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Assenzio", "79798564", "assy@gmail.com", bergamo, torino, 0.68));
		offers.add(createRideOffer("Astronzio", "435657", "stronzy@gmail.com", udine, palermo, 0.67));
		offers.add(createRideOffer("Affare", "0411231226", "affa@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alimortaccitua", "02327657", "morty@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Bucio", "041997626", "bano@gmail.com", trapani, palermo, 0.93));
		offers.add(createRideOffer("Bieco", "03376327", "bieco@gmail.com", venezia, roma, 0.54));
		offers.add(createRideOffer("Bieno", "0411231226", "by@gmail.com", trieste, trapani, 0.76));
		offers.add(createRideOffer("Busone", "057567657", "busy@gmail.com", pisa, palermo, 0.43));
		offers.add(createRideOffer("Boh", "79798564", "boh@gmail.com", torino, milano, 0.98));
		offers.add(createRideOffer("Bino", "435657", "bino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Baucco", "0411231226", "bau@gmail.com", napoli, bergamo, 0.43));
		offers.add(createRideOffer("Zeno", "02327657", "zen@gmail.com", avellino, firenze, 0.59));
		
		offers.add(createRideOffer("Gigio", "041997626", "gigio@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Gino", "03376327", "gino@gmail.com", roma, venezia, 0.34));
		offers.add(createRideOffer("Giovanni", "0411231226", "gio@gmail.com", napoli, trieste, 0.76));
		offers.add(createRideOffer("Luca", "057567657", "luca@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Fabio", "79798564", "fabio@gmail.com", milano, trieste, 0.98));
		offers.add(createRideOffer("Dino", "435657", "dino@gmail.com", trieste, palermo, 0.67));
		offers.add(createRideOffer("Albino", "0411231226", "albino@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alfredo", "02327657", "alfredo@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Gigietto", "041997626", "gigio@gmail.com", venezia, palermo, 0.93));
		offers.add(createRideOffer("Ginetto", "03376327", "gino@gmail.com", venezia, roma, 0.34));
		offers.add(createRideOffer("Giovannino", "0411231226", "gio@gmail.com", trieste, napoli, 0.76));
		offers.add(createRideOffer("Luchetto", "057567657", "luca@gmail.com", milano, palermo, 0.73));
		offers.add(createRideOffer("Fabioetto", "79798564", "fabio@gmail.com", trieste, milano, 0.98));
		offers.add(createRideOffer("Dinetto", "435657", "dino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Albinino", "0411231226", "albino@gmail.com", napoli, milano, 0.43));
		offers.add(createRideOffer("Alfredino", "02327657", "alfredo@gmail.com", avellino, firenze, 0.53));
		offers.add(createRideOffer("Alberto", "041997626", "baby@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Alfio", "03376327", "alfy@gmail.com", roma, treviso, 0.44));
		offers.add(createRideOffer("Arlechino", "0411231226", "arle@gmail.com", latina, trieste, 0.76));
		offers.add(createRideOffer("Artemide", "057567657", "arty@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Assenzio", "79798564", "assy@gmail.com", bergamo, trieste, 0.68));
		offers.add(createRideOffer("Astronzio", "435657", "stronzy@gmail.com", udine, palermo, 0.67));
		offers.add(createRideOffer("Affare", "0411231226", "affa@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alimortaccitua", "02327657", "morty@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Banano", "041997626", "bano@gmail.com", treviso, palermo, 0.93));
		offers.add(createRideOffer("Bieco", "03376327", "bieco@gmail.com", venezia, roma, 0.54));
		offers.add(createRideOffer("Bieno", "0411231226", "by@gmail.com", trieste, trapani, 0.76));
		offers.add(createRideOffer("Busone", "057567657", "busy@gmail.com", milano, palermo, 0.43));
		offers.add(createRideOffer("Boh", "79798564", "boh@gmail.com", udine, milano, 0.98));
		offers.add(createRideOffer("Bino", "435657", "bino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Baucco", "0411231226", "bau@gmail.com", napoli, bergamo, 0.43));
		offers.add(createRideOffer("Balordo", "02327657", "baloo@gmail.com", avellino, firenze, 0.59));
		
		offers.add(createRideOffer("Claudio", "041997626", "cla@gmail.com", treviso, venezia, 0.93));
		offers.add(createRideOffer("Candido", "03376327", "can@gmail.com", roma, avellino, 0.77));
		offers.add(createRideOffer("Clodio", "0411231226", "clo@gmail.com", napoli, trieste, 0.76));
		offers.add(createRideOffer("Cristian", "057567657", "cri@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Cromo", "79798564", "cro@gmail.com", torino, trieste, 0.98));
		offers.add(createRideOffer("Cristiano", "435657", "dino@gmail.com", trieste, palermo, 0.67));
		offers.add(createRideOffer("Clara", "0411231226", "cla@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alfredo", "02327657", "alfredo@gmail.com", firenze, napoli, 0.66));
		offers.add(createRideOffer("Gigietto", "041997626", "gigio@gmail.com", venezia, palermo, 0.93));
		offers.add(createRideOffer("Ginetto", "03376327", "gino@gmail.com", torino, roma, 0.34));
		offers.add(createRideOffer("Giovannino", "0411231226", "gio@gmail.com", trieste, napoli, 0.76));
		offers.add(createRideOffer("Luchetto", "057567657", "luca@gmail.com", milano, palermo, 0.73));
		offers.add(createRideOffer("Fabietto", "79798564", "fabio@gmail.com", trieste, milano, 0.98));
		offers.add(createRideOffer("Fnocchio", "435657", "dino@gmail.com", palermo, trieste, 0.89));
		offers.add(createRideOffer("Albinino", "0411231226", "albino@gmail.com", napoli, milano, 0.43));
		offers.add(createRideOffer("Alfredino", "02327657", "alfredo@gmail.com", avellino, firenze, 0.53));
		offers.add(createRideOffer("Alberto", "041997626", "baby@gmail.com", palermo, venezia, 0.93));
		offers.add(createRideOffer("Alfio", "03376327", "alfy@gmail.com", roma, treviso, 0.44));
		offers.add(createRideOffer("Arlechino", "0411231226", "arle@gmail.com", latina, trieste, 0.76));
		offers.add(createRideOffer("Artrite", "057567657", "arty@gmail.com", palermo, milano, 0.73));
		offers.add(createRideOffer("Assenzio", "79798564", "assy@gmail.com", bergamo, torino, 0.68));
		offers.add(createRideOffer("Astronzio", "435657", "stronzy@gmail.com", udine, palermo, 0.67));
		offers.add(createRideOffer("Affare", "0411231226", "affa@gmail.com", milano, napoli, 0.43));
		offers.add(createRideOffer("Alimortaccitua", "02327657", "morty@gmail.com", firenze, napoli, 0.53));
		offers.add(createRideOffer("Bucio", "041997626", "bano@gmail.com", trapani, palermo, 0.93));
		offers.add(createRideOffer("Bieco", "03376327", "bieco@gmail.com", venezia, roma, 0.54));
		offers.add(createRideOffer("Bieno", "0411231226", "by@gmail.com", trieste, trapani, 0.76));
		offers.add(createRideOffer("Busone", "057567657", "busy@gmail.com", pisa, palermo, 0.43));
		offers.add(createRideOffer("Boh", "79798564", "boh@gmail.com", torino, milano, 0.98));
		offers.add(createRideOffer("Bino", "435657", "bino@gmail.com", palermo, trieste, 0.67));
		offers.add(createRideOffer("Baucco", "0411231226", "bau@gmail.com", napoli, bergamo, 0.43));
		offers.add(createRideOffer("Zeno", "02327657", "zen@gmail.com", avellino, firenze, 0.59));
		return offers;
	}
	
	public static Location createLocation(String name, int x, int y) {
		
		Location lo = new Location();
		lo.setName(name);
		lo.setX_coordinate(x);
		lo.setY_coordinate(y);
		return lo;
	}
}
